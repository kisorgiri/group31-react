// html 5
// <!DOCTYPE html>


// angle bracket <>
// angle bracket enclosed keyword are element (tags) of html
// element will have their own behaviour to render in window

// we will have one root element in html
// parent element
// child element


// element // element //element
// attribute==> properities that defines element

// paragraph
// header
// data collection
// forms
// data visualtion
// list
// table

// img
// css 
// 


// ELEMENT 
// INLINE ELEMENT
// all those element which take their required width and never start from new line  
// BLOCK ELEMENT
// all those element whih take entire width of window  and always starts from new line


// CSS cascading style sheet
// its purpose is to style the markup
// three ways of implementing css

// inline css
// internal css
// external css

// REDUX
// state management tool

// MVC ==> 
// models views and controller
// bidirectional data flow from models and controller

// flux architecture
// Views, actions, dispatchers, store
// unidirectional data flow
// store contatins centralized state and its update logic
// there can be multiple store


// REDUX
// views ==> actions ==> reducers ==> store has state and state defines UI
// unidirectional data flow
// reducer contains logic to update store
// store contains state
// there can be only one store

// redux 
// store, actions and reducer

// library
// redux ==> store
// react-redux ==> provider ==> attch store with react application
//             ==> connect ==> component ko connection with redux

// two object are used when connectings
// mapStateToProps
// store ko data as an props
// mapDispatchedToProps
// actions ko function as an props


// redux-thunk ===> action functions will return another function with dispatch




