import { toast } from 'react-toastify';

const showSuccess = (msg) => {
    toast.success(msg);
}
const showInfo = (msg) => {
    toast.info(msg);
}
const showWarnings = (msg) => {
    toast.warning(msg);
}
const showError = (msg) => {
    toast.error(msg);
}

const handleError = (error) => {
    debugger;
    let errMsg = 'something went wrong';
    let err = error && error.response;
    if (err && err.data) {
        errMsg = err.data.msg;
    }
    return showError(errMsg);
    // step 1 CHeck Error
    // parse Error
    // extract/prepare error message
    // show them in UI
}

const notify = {
    showSuccess,
    showInfo,
    showWarnings,
    handleError
}

export default notify;
