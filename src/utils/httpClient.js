import axios from 'axios';
const BASE_URL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json'
});

const getHeaders = (isSecured = false) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecured) {
        options['Authorization'] = localStorage.getItem('token');
    }

    return options;

}

/**
 * http post request method
 * @param {string} url 
 * @param {object} data 
 * @param {boolen} isSecured 
 * @param {object} [params] 
 * @returns {Promise}
 */

const POST = (url, data, isSecured, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecured),
        params
    })
}

const GET = (url, isSecured, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecured),
        params
    })
}

const PUT = (url, data, isSecured, params = {}) => {
    return http.put(url, data, {
        headers: getHeaders(isSecured),
        params
    })
}

const DELETE = (url, isSecured, params = {}) => {
    return http.delete(url, {
        headers: getHeaders(isSecured),
        params
    })
}

const UPLOAD = (method, url, data, files) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        // append images in formdata
        if (files.length) {
            files.forEach((item, index) => {
                formData.append('image', item, item.name)
            })
        }

        // append textual data in formdata
        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.response);
                }
            }
        }

        xhr.open(method, `${BASE_URL}${url}?token=${(localStorage.getItem('token'))}`, true);
        xhr.send(formData);
    })

}

const httpClient = {
    POST,
    GET,
    PUT,
    DELETE,
    UPLOAD
}
export default httpClient;
