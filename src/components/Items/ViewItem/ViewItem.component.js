import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { formatDate } from '../../../utils/dateProcessing';
import { Loader } from '../../Common/Loader/Loader.component';
import { connect } from 'react-redux';
import { fetchItems_ac, removeItem_ac } from '../../../actions/items/item.ac';

const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewItemComponent extends Component {
    constructor() {
        super()
    }

    componentDidMount() {
        console.log('this.props  in view component>>', this.props)
        if (this.props.itemData) {
            return this.setState({
                items: this.props.itemData
            })
        }
        this.props.fetch();
    }

    remove = (id, i) => {
        // ask for confirmation
        const confirmation = window.confirm('Are you sure to remove?');

        if (confirmation) {
            this.props.remove(id, i)
        }
    }

    edit = (id) => {
        this.props.history.push(`/edit_item/${id}`)
    }

    render() {
        let content = this.props.isLoading
            ? <Loader></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.items.map((item, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td> <Link to={`item_details/${item._id}`}> {item.name}</Link> </td>
                                <td>{item.category}</td>
                                <td>{item.price}</td>
                                <td>{formatDate(item.createdAt)}</td>
                                <td>
                                    <img src={`${IMG_URL}${item.images[0]}`} alt="item_image" width="200px"></img>
                                </td>
                                <td>
                                    <button className="btn btn-info" onClick={() => this.edit(item._id)}>edit</button>
                                    <button className="btn btn-danger" onClick={() => this.remove(item._id, index)}>delete</button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        return (
            <>
                <h2>View Items</h2>
                {this.props.itemData && <button className="btn btn-success" onClick={this.props.searchAgain}>search again</button>}
                {content}
            </>
        )
    }
}

// 1st object
const mapStateToProps = rootStore => ({
    isLoading: rootStore.items.isLoading,
    items: rootStore.items.items
})
// incoming data from store inside component as props

// 2nd object
// map dispatch to props
const mapDispatchToProps = dispatch => ({
    fetch: () => { dispatch(fetchItems_ac()) },
    remove: (id, i) => { dispatch(removeItem_ac(id, i)) }
})
// trigger garne part
// actions files functions form actions inisde this componet as props

export const ViewItem = connect(mapStateToProps, mapDispatchToProps)(ViewItemComponent)
