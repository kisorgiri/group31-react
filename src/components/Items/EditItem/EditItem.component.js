import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient';
import notify from '../../../utils/notify';
import { Loader } from '../../Common/Loader/Loader.component';
import { ItemForm } from '../ItemForm/ItemForm.component';

export class EditItem extends Component {
    constructor() {
        super()

        this.state = {
            item: {},
            isSubmitting: false,
            isLoading: false
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.itemId = this.props.match.params['id'];
        httpClient.GET(`/item/${this.itemId}`, true)
            .then(response => {
                this.setState({
                    item: response.data
                })

            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient
            .UPLOAD('PUT', `/item/${data._id}`, data, files)
            .then(response => {
                notify.showInfo("Item Updated");
                this.props.history.push('/view_items');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ItemForm
                title="Edit Item"
                isSubmitting={this.state.isSubmitting}
                submitCallback={this.edit}
                itemData={this.state.item}
            ></ItemForm>
        return content;
    }
}
