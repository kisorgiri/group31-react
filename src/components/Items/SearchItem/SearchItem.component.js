import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient'
import notify from '../../../utils/notify'
import { Button } from './../../Common/Button/Button.component'
import { ViewItem } from './../ViewItem/ViewItem.component';

const defaultForm = {
    category: '',
    name: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    tags: '',
    offers: '',
    multipleDateRange: ''
}
export class SearchComponent extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false,
            allItems: [],
            categories: [],
            names: [],
            searchResults: []
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        httpClient.POST('/item/search', {})
            .then(response => {
                let categories = [];
                response.data.forEach((item, index) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                });

                this.setState({
                    categories,
                    allItems: response.data
                })

            })
            .catch(err => {
                notify.handleError(err);

            })
            .finally(() => {
                this.setState({
                    isLoading: true
                })
            })
    }

    handleChange = (e) => {

        let { type, name, value, checked } = e.target;
        if (name === 'category') {
            this.buildNameList(value);
        }

        if (type === 'checkbox') {
            value = checked
        }

        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    handleSubmit = e => {
        e.preventDefault();

        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate
        }
        httpClient.POST('/item/search', data)
            .then(response => {
                if (!response.data.length) {
                    return notify.showInfo("No any item matched your search query");
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    buildNameList = category => {
        const { allItems } = this.state;
        const names = allItems.filter(item => item.category === category);
        this.setState({
            names
        })
    }

    searchAgain = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defaultForm
            }
        })
    }

    validateForm = fieldName => {

        let errMsg = this.state.data[fieldName]
            ? ''
            : `${fieldName} is required`

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })

    }

    render() {
        let content = this.state.searchResults.length > 0
            ? <ViewItem
                itemData={this.state.searchResults}
                searchAgain={this.searchAgain}
            ></ViewItem>
            : <>
                <h2>Search Item</h2>
                <form className="form-group" noValidate onSubmit={this.handleSubmit}>
                    <label>Category</label>
                    <select name="category" value={this.state.data.category} onChange={this.handleChange} className="form-control">
                        <option value="">(Select Item)</option>
                        {
                            this.state.categories.map((item, index) => (
                                <option key={index} value={item}>
                                    {item}
                                </option>
                            ))
                        }
                    </select>
                    <p className="error">{this.state.error.category}</p>
                    {
                        this.state.data.category && (
                            <>
                                <label>name</label>
                                <select name="name" value={this.state.data.name} onChange={this.handleChange} className="form-control">
                                    <option value="">(Select Name)</option>
                                    {
                                        this.state.names.map((item, index) => (
                                            <option key={index} value={item.name}>
                                                {item.name}
                                            </option>
                                        ))
                                    }
                                </select>
                                <p className="error">{this.state.error.name}</p>

                            </>
                        )
                    }
                    <label>Min Price</label>
                    <input className="form-control" type="number" name="minPrice" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input className="form-control" type="number" name="maxPrice" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input className="form-control" type="date" name="fromDate" onChange={this.handleChange}></input>
                    <input type="checkbox" onChange={this.handleChange} name="multipleDateRange"></input>
                    <label>Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input className="form-control" type="date" name="toDate" onChange={this.handleChange}></input>
                            </>
                        )
                    }

                    <label>Tags</label>
                    <input className="form-control" type="text" placeholder="Tags" name="tags" onChange={this.handleChange}></input>

                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    >

                    </Button>
                </form>

            </>
        return content
    }
}
