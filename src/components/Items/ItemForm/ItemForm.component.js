import React, { Component } from 'react'
import { Button } from '../../Common/Button/Button.component'

const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
    name: '',
    description: '',
    price: '',
    category: '',
    color: '',
    brand: '',
    size: '',
    isReturnEligible: '',
    returnTimePeroidInDay: '',
    modelNo: '',
    offers: '',
    tags: '',
    quantity: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
}

export class ItemForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm,
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            isValidForm: false
        }
    }

    componentDidMount() {
        if (this.props.itemData) {
            const { itemData } = this.props;
            this.setState({
                data: {
                    ...defaultForm,
                    ...itemData,
                    discountedItem: itemData.discount && itemData.discount.discountedItem ? itemData.discount.discountedItem : '',
                    discountType: itemData.discount && itemData.discount.discountType ? itemData.discount.discountType : '',
                    discountValue: itemData.discount && itemData.discount.discountValue ? itemData.discount.discountValue : '',

                }
            })
        }
    }

    handleChange = (e) => {
        let { name, type, value, checked, files } = e.target;


        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0]);
            return this.setState({
                filesToUpload: filesToUpload
            });
        }

        if (type === 'checkbox') {
            value = checked;
        }

        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {

            case 'category':
            case 'name':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : `${fieldName} is required*`
                break;

            default:
                break;

        }
        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })

    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }
    render() {
        const { data, error } = this.state;
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" value={data.name} placeholder="Name" onChange={this.handleChange}></input>
                    <p className="error">{error.name}</p>

                    <label>Description</label>
                    <textarea
                        value={data.description}
                        className="form-control"
                        name="description"
                        onChange={this.handleChange}
                        placeholder="Description goes here"
                        rows={5}
                    ></textarea>
                    <label>Category</label>
                    <input type="text" className="form-control" name="category" value={data.category} placeholder="Category" onChange={this.handleChange}></input>
                    <p className="error">{error.category}</p>
                    <label>Brand</label>
                    <input type="text" className="form-control" name="brand" value={data.brand} placeholder="Brand" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input type="number" className="form-control" name="price" value={data.price} placeholder="Price" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" className="form-control" name="color" value={data.color} placeholder="Color" onChange={this.handleChange}></input>
                    <label>Model No.</label>
                    <input type="text" className="form-control" name="modelNo" value={data.modelNo} placeholder="Model No." onChange={this.handleChange}></input>
                    <label>Size</label>
                    <input type="text" className="form-control" name="size" value={data.size} placeholder="Size" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={data.discountedItem} name="discountedItem" onChange={this.handleChange}></input>
                    <label> Discounted Item </label>
                    <br />
                    {
                        this.state.data.discountedItem && (
                            <>
                                <label> Discount Type </label>
                                <select className="form-control" name="discountType" value={data.discountType} onChange={this.handleChange}>
                                    <option value="">(Select Discount Type)</option>
                                    <option value="percentage">Percentage</option>
                                    <option value="quantity">Quantity</option>
                                    <option value="value">Value</option>
                                </select>
                                <label>Disocunt Value</label>
                                <input type="text" className="form-control" value={data.discountValue} name="discountValue" placeholder="Disocunt Value" onChange={this.handleChange}></input>
                            </>
                        )
                    }

                    <input type="checkbox" checked={data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange}></input>
                    <label>Warrenty Item</label>
                    <br />
                    <label>Warrenty Peroid</label>
                    <input type="text" className="form-control" value={data.warrentyPeroid} name="warrentyPeroid" placeholder="Warrenty Peroid" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={data.isReturnEligible} name="isReturnEligible" onChange={this.handleChange}></input>
                    <label>Return Eligible</label>
                    <br />
                    <label>Return Time In Day</label>
                    <input type="text" className="form-control" value={data.returnTimePeroidInDay} name="returnTimePeroidInDay" placeholder="Return Time In Day" onChange={this.handleChange}></input>

                    <label>Tags</label>
                    <input type="text" className="form-control" name="tags" value={data.tags} placeholder="Tags" onChange={this.handleChange}></input>
                    <label>Offers</label>
                    <input type="text" className="form-control" name="offers" value={data.offers} placeholder="Offers" onChange={this.handleChange}></input>
                    {
                        this.props.itemData
                        && this.props.itemData.images
                        && this.props.itemData.images.length > 0
                        && (
                            <>
                                <label>Previous Image</label>
                                <br />
                                <img src={`${IMG_URL}${this.props.itemData.images[0]}`} alt="item_image.png" width="400px"></img>
                                <br />
                            </>
                        )
                    }
                    <label>Choose Image</label>
                    <input type="file" className="form-control" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={this.props.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>
                </form>
            </>
        )
    }
}
