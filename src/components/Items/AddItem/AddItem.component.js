import React, { Component } from 'react'
import { ItemForm } from '../ItemForm/ItemForm.component'
import httpClient from './../../../utils/httpClient'
import notify from './../../../utils/notify'

export class AddItem extends Component {

    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        // httpClient.POST('/item', data, true)
        httpClient.UPLOAD('POST', '/item', data, files)
            .then(response => {
                notify.showInfo("Item Added Successfully")
                this.props.history.push("/view_items")
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <ItemForm
                title="Add Item"
                submitCallback={this.add}
                isSubmitting={this.state.isSubmitting}
            ></ItemForm>
        )
    }
}
