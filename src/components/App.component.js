import React from 'react';
import { AppRouting } from './App.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { store } from './../store';
import { Provider } from 'react-redux'
export const App = () => {
    // args or incoming data are props
    // function must return single html node
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
            </Provider>
            <ToastContainer />
        </div>

    )
}

// component==> components are basic buidling block of react
// component always returns a html node
// component can be written in two ways
// 1.functional component
// return is mandatory in a functional component
// 2. class based component
// it should be extended from react's component class
// it must have a render method
// render must return a single html node


// props and state
// props ==> incoming data inside a component are props

// state ==> component data that are maintained inside a component

// throughout this course
// if we have to maintain state ===> class based component
// if we doesnot have to maintain state ==> functional component

// stateless component ==> functional component
// stateful component ===> class based component

