import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import notify from './../../../utils/notify';
import httpClient from './../../../utils/httpClient'
import { Button } from '../../Common/Button/Button.component';

// class based component
const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {

    constructor() {
        super();
        // to maintain state
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false
        };
    }
    componentDidMount() {
        if (localStorage.getItem('remember_me') === 'true') {
            this.props.history.push('/dashboard')
        }
    }

    handleChange = e => {
        const { type, name, value, checked } = e.target;
        // to update state we must use setState method
        if (type === 'checkbox') {
            this.setState({
                remember_me: checked
            })
            return;
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    validateForm = fieldName => {
        let errMsg = this.state.data[fieldName]
            ? ''
            : 'required field*'

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }


    handleSubmit = e => {
        e.preventDefault();
        // API call
        httpClient.POST(`/auth/login`, this.state.data)
            .then(response => {
                // response==> data ==> user ==> role
                // role if admin==> admin dashboard
                // role if accounts ==> account Dashboard
                // role if hr ==> hr dashboard

                notify.showSuccess(`Welcome ${response.data.user.username}`);
                localStorage.setItem('remember_me', this.state.remember_me);
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user))

                this.props.history.push('/dashboard')

            })
            .catch(err => {
                notify.handleError(err);
            })

    }
    // render method is mandatory
    render() {
        // it must return single html node
        // UI logic can be kept inside render
        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" value={this.state.data.username} name="username" id="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.usernameErr}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.passwordErr}</p>
                    <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                    <label>Remember Me</label>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                        enabledLabel="Login"
                        disabledLabel="Logingin ..."
                    ></Button>
                </form>
                <p>Don't have an account?</p>
                <p style={{ float: 'left' }}>Register<Link to="/register"> here</Link></p>
                <p style={{ float: 'right' }}><Link to="/forgot_password"> Forgot Password</Link></p>
            </div>
        )
    }
}
