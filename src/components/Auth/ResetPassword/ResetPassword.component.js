import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient'
import notify from '../../../utils/notify'
import { Button } from '../../Common/Button/Button.component'

export class ResetPassword extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                password: '',
                confirmPassword: ''
            },
            error: {
                password: '',
                confirmPassword: ''
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    componentDidMount() {
        this.id = this.props.match.params['id'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {
            case 'password':
                errMsg = this.state.data['confirmPassword']
                    ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field *';
                break;
            case 'confirmPassword':
                errMsg = this.state.data['password']
                    ? this.state.data['password'] === this.state.data[fieldName]
                        ? ''
                        : 'password did not match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;

            default:
                break;
        }


        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST(`/auth/reset-password/${this.id}`, this.state.data)
            .then(response => {
                notify.showInfo("Password reset successfull,please login");
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <>
                <h2>Reset Password</h2>
                <p>PLease choose your password</p>
                <form className="form-group" noValidate onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input className="form-control" type="password" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input className="form-control" type="password" name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}>

                    </Button>
                </form>
            </>
        )
    }
}
