import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import notify from './../../../utils/notify';
import httpClient from '../../../utils/httpClient';
import { Button } from '../../Common/Button/Button.component';

const defaultForm = {
    name: '',
    email: '',
    tempAddress: '',
    permanentAddress: '',
    username: '',
    password: '',
    confirmPassword: '',
    gender: '',
    dob: '',
    phoneNumber: ''
}

export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        };
    }

    handleChange = (e) => {

        const { name, value } = e.target;
        // this.setState({
        //     data: {
        //         [name]: value
        //     }
        // }, () => {
        //     console.log("this.state >>", this.state)
        // })
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Username must be 6 characters long'
                    : 'required field*'
                break;
            case 'password':
                errMsg = this.state.data['confirmPassword']
                    ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field *';
                break;
            case 'confirmPassword':
                errMsg = this.state.data['password']
                    ? this.state.data['password'] === this.state.data[fieldName]
                        ? ''
                        : 'password did not match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient
            .POST(`/auth/register`, this.state.data)
            .then(response => {
                // console.log('http response >>', response)
                this.props.history.push('/');
                notify.showSuccess("Registration Successful, Please Login");

            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: true
                })
            })

    }

    render() {
        return (
            <div>
                <h2>Register</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input className="form-control" type="text" name="name" placeholder="Name" onChange={this.handleChange}></input>
                    <label>Email</label>
                    <input className="form-control" type="text" name="email" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <label>Username</label>
                    <input className="form-control" type="text" name="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>

                    <label>Password</label>
                    <input className="form-control" type="password" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input className="form-control" type="password" name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <label>Phone Number</label>
                    <input className="form-control" type="number" name="phoneNumber" onChange={this.handleChange}></input>
                    <label>D.O.B</label>
                    <input className="form-control" type="date" name="dob" onChange={this.handleChange}></input>
                    <label>Gender</label>
                    <input type="text" className="form-control" name="gender" onChange={this.handleChange}></input>
                    <label>Temporary Address</label>
                    <input type="text" className="form-control" placeholder="Temporary Address" name="tempAddress" onChange={this.handleChange}></input>
                    <label>Permanent Address</label>
                    <input type="text" className="form-control" placeholder="Permanent Address" name="permanentAddress" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>

                </form>
                <p>Already Registered?</p>
                <p>
                    <Link to="/">back to login</Link>
                </p>
            </div>
        )
    }
}
