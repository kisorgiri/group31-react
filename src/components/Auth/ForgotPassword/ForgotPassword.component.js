import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient'
import notify from '../../../utils/notify'
import { Button } from '../../Common/Button/Button.component'

export class ForgotPassword extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                email: ''
            },
            error: {
                email: ''
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            data: {
                [name]: value
            }
        }, () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg = this.state.data[fieldName]
            ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                ? ''
                : 'invalid email'
            : 'required field*'

        this.setState({
            error: {
                [fieldName]: errMsg
            }
        }, () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', this.state.data)
            .then(response => {
                notify.showInfo("Password reset link sent to your inbox please check inbox");
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <>
                <h2>Forgot Password</h2>
                <p>PLease provide your email address to reset your password</p>
                <form className="form-group" noValidate onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}>

                    </Button>
                </form>
            </>
        )
    }
}
