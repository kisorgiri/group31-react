import React from 'react';
import { Link } from 'react-router-dom';
import './SideBar.component.css';

export const SideBar = () => {
    return (
        <div className="sidenav">
            <Link to="/dashboard">Dashboard</Link>
            <Link to="/add_item">Add Item</Link>
            <Link to="/view_items">View Items</Link>
            <Link to="/search">Search Items</Link>
            <hr></hr>
            <Link to="/chat">Messages</Link>
        </div>
    )
}
