import React from 'react';
import './NavBar.component.css'
import { Link, withRouter } from 'react-router-dom';

const NavBarComponent = (props) => {
    const logout = () => {
        localStorage.clear();
        props.history.push('/');
        // clear localstorage
        // navigate to login page
    }
    let menu = props.isLoggedIn
        ? <ul className="navList">
            <li className="navItem">
                <Link to="/dashboard">Dashboard</Link>

            </li>
            <li className="navItem">
                <Link to="/contact">Contact</Link>

            </li>
            <li className="navItem">
                <button onClick={logout} className="btn btn-success logout">Logout</button>
            </li>
        </ul>
        : <ul className="navList">
            <li className="navItem">
                <Link to="/home">Home</Link>
            </li>

            <li className="navItem">
                <Link to="/register">Register</Link>

            </li>
            <li className="navItem">
                <Link to="/">Login</Link>
            </li>
        </ul>
    return (
        <div className="navBar">
            {menu}
        </div>
    )
}

export const NavBar = withRouter(NavBarComponent);
