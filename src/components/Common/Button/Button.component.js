import React from 'react'

export const Button = (props) => {
    const enabledLabel = props.enabledLabel || 'submit';
    const disabledLabel = props.disabledLabel || 'submiting...';
    let btn = props.isSubmitting
        ? <button disabled className="btn btn-info m-t-10">{disabledLabel}</button>
        : <button disabled={!props.isValidForm} type="submit" className="btn btn-primary m-t-10">{enabledLabel}</button>
    return btn;
}
