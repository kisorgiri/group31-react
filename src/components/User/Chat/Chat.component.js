import React, { Component } from 'react'
import * as io from 'socket.io-client';

export class Chat extends Component {
    constructor() {
        super()

        this.state = {
            messageBody: {
                sender: '',
                senderId: '',
                receiver: '',
                receiverId: '',
                message: '',
                time: ''
            },
            messages: [],
            users: []
        }
    }
    componentDidMount() {
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.runSocket();
    }

    runSocket = () => {
        this.socket.on('hello', (data) => {
            console.log('message in hello envent >>', data);
            this.socket.emit('hi', 'hi from react')
        })

        this.socket.on('reply-msg-own', (data) => {
            // message ma push garne
        })

        this.socket.on('reply-msg-others', (data) => {
            // message ma push garne

        })
    }

    handleSubmit = e => {
        e.preventDefault();
        const { messageBody } = this.state;
        // todo prepare messagebody
        this.socket.emit('new-message', messageBody);
    }

    render() {
        return (
            <>
                <h2>Let's Chat</h2>
            </>
        )
    }
}
