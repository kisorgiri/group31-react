// routing related stuff
import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { NavBar } from './Common/NavBar/NavBar.component';
import { SideBar } from './Common/SideBar/SideBar.component';
import { AddItem } from './Items/AddItem/AddItem.component';
import { EditItem } from './Items/EditItem/EditItem.component';
import { SearchComponent } from './Items/SearchItem/SearchItem.component';
import { ViewItem } from './Items/ViewItem/ViewItem.component';
import { Chat } from './User/Chat/Chat.component';

const Home = (props) => {
    // console.log('props in home >>', props)
    return <h2>Home Page</h2>
}
const Dashboard = (props) => {
    // console.log('props in dashboard', props)
    return (
        <p>Please use side navigation menu or contact system administrator for suppport</p>
    )
}
const About = (props) => {
    return <h2>About Page</h2>
}
const Contact = (props) => {
    return <h2>Contact Page</h2>
}

const NotFound = (props) => {
    return (
        <div>
            <h3>Not Found</h3>
            <img src='images/not-found.jpg' width="600px"></img>
        </div>
    )
}


// protectedRoute
const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => (
            localStorage.getItem('token')
                ? <div>
                    <NavBar isLoggedIn={true}></NavBar>
                    <SideBar></SideBar>
                    <div className="main">
                        <Component {...routeProps} />
                    </div>
                </div>
                : <Redirect to="/" />
        )}></Route>
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => (
            <div>
                <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
                <div className="main">
                    <Component {...routeProps} />
                </div>
            </div>
        )}></Route>
    )
}


export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register}></PublicRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>
                <PublicRoute path="/about" component={About}></PublicRoute>
                <ProtectedRoute path="/contact" component={Contact}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_item" component={AddItem}></ProtectedRoute>
                <ProtectedRoute path="/view_items" component={ViewItem}></ProtectedRoute>
                <ProtectedRoute path="/edit_item/:id" component={EditItem}></ProtectedRoute>
                <ProtectedRoute path="/search" component={SearchComponent}></ProtectedRoute>
                <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset_password/:id" component={ResetPassword}></PublicRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>

        </BrowserRouter>
    )
}
// new Array(message)
// 
// console.log('message');
// res.json(message)
// res.send(JSON.stringify(errors));



// summary
// BrowserRouter
// Route
// Link
// Switch
// 
// Props
// history
// match
// location
