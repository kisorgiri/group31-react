// export const fetch_items_ac = (params) => {
//     return (dispatch) => {
//         // delay dispatch way
//     }
// }

import httpClient from "../../utils/httpClient";
import notify from "../../utils/notify";
import { ITEMS_RECEIVED, ITEM_REMOVED, SET_IS_LOADING } from "./types"

// function fetch(condition) {
//     return function (dispatch) {

//     }
// }

export const fetchItems_ac = params => dispatch => {
    console.log('at action file');
    dispatch({
        type: SET_IS_LOADING,
        payload: true
    })


    httpClient.GET('/item', true)
        .then(response => {
            dispatch({
                type: ITEMS_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch({
                type: SET_IS_LOADING,
                payload: false
            })
        })
}

export const removeItem_ac = (id, i) => (dispatch, getState) => {

    httpClient.DELETE(`/item/${id}`, true)
        .then(response => {
            notify.showSuccess("Item Removed");
            const items = getState().items.items;
            items.splice(i, 1);
            dispatch({
                type: ITEM_REMOVED,
                items
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
}
