import { ITEMS_RECEIVED, ITEM_REMOVED, SET_IS_LOADING } from "../actions/items/types"

const defaultState = {
    isLoading: false,
    items: []
}

export const itemReducer = (state = defaultState, action) => {
    console.log('at reducer ', action);
    switch (action.type) {

        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }

        case ITEMS_RECEIVED:
            return {
                ...state,
                items: action.payload
            }

        case ITEM_REMOVED:
            return {
                ...state,
                items: [...action.items]
            }

        default:
            return {
                ...state
            }
    }
}
