import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// import reducer
import rootReducer from './reducers';

const middleware = [thunk]
// initial state
const initialState = {
    items: {
        isLoading: false,
        items: [],
        isSubmitting: false,
        item: {}
    },
    // notification: {
    //     isLoading: false,
    //     notifications: [],
    //     isSubmitting: false,
    //     notification: {}
    // },
    // users: {
    //     users: [],
    //     isLoading: false,
    //     isSubmitting: false,
    //     user: {}
    // }
}

export const store = createStore(rootReducer, initialState, applyMiddleware(
    ...middleware
))
