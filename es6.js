// let vs var(block scope)
// Object shorthand
// Object Destruct
// arrow notation function
// default argument
// spread operator 
// rest operator
// template literals
// class
// import and export

// 1.block scope
// global scope and functional scope
// function vitra pani block wise scope seperation
// let holds block scope and var holds functional scope

// Object Shorthand
// var name = 'kishor giri';
// var address = 'tinkune';

// var obj = {
//     phone: 222,
//     email: 'jskisor@gmail.com',
//     name,
//     address
// }

// console.log('obj is >>',obj);

// Object destruct
// function goToMarket(amt) {
//     var fruits = ['apple', 'mango'];
//     var vegitables = ['potato'];

//     return {
//         fruits,
//         vegitables,
//         fish: 'fish',
//         oil: 'oil',
//         sweets: 'abcd sweets',
//         abc: 'xyz'
//     }
// }
// var { sweets } = goToMarket(); // destrruct

// console.log('sweets is >>',sweets)

// var myObj = {
//     fish: 'fish',
//     oil: 'oil',
//     sweets: 'abcd sweets',
//     abc: 'xyz'
// }

// // var { fish, oil, kishor } = myObj;
// // console.log('fish is >',kishor)

// const { fish: Kishor } = myObj;
// console.log('fish is >', Kishor)


// arrow notation function
// function welcome(name) {
//     // it will start its own this
//     return 'hi and welcome';
// }

// const welcome = (name) => {
//     return 'hi and welcome'
// }

// const welcome = name => {
//     // single argument huda parenthesis rakhnu pardaina
//     return 'hi and welcome'
// }

// const welcome = name => 'hi and welecome';
// // inline function
// without {} and with return also without parenthesis

// arrow notation function will inherit parent this

// var laptops = [{
//     name: 'dell',
//     generation: 'i7'
// },
// {
//     name: 'machbook',
//     generation: 'i9'
// },
// {
//     name: 'hp',
//     generation: 'i5'
// },
// {
//     name: 'acer',
//     generation: 'i7'
// }]

// // var i7Laltop = laptops.filter(function (item) {
// //     if (item.generation === 'i7') {
// //         return true;
// //     }
// // })
// var i7Laltop = laptops.filter(item => item.generation === 'i7');
// console.log('i7 latptops >>', i7Laltop)

// var obj = {
//     name: 'kishor',
//     name: 'shyam',
//     age: '33',
//     age: '39',
// }

// var obj1 = {
//     name: 'sujan',
//     age: '39',
// }
// console.log('obj >>', obj)

// const { name } = obj1

// function sendMail(details = { email: 'test' }) {

//     console.log('details is .>', details.email);
// }

// sendMail();

// function welcome(name, place = 'broadway') {

// }


// spread operator
// syntax ...
// spread operator can be used in array and object
// var email = {
//     from: 'kishor',
//     to: 'bibek'
// }
// var address = {
//     temp_addr: 'bkt',
//     permanent_addr: 'patan'
// }

// var final = {
//     ...email,
//     ...address
// }

// var email1 = {
//     ...email,
//     subject:'hello',
//     from:'nothig'
// };
// email.sub = 'hi';
// console.log('email is >>', final);
// console.log('email1', email1);

// REST operator
// syntax (...)

// const market = {
//     fish: 'abcd xyz',
//     vegitables: 'potato',
//     fruits: 'apple',
//     goods: 'goods'
// }

// const { fish: favFood, ...abcd } = market;

// console.log('favFood >>', favFood);
// console.log('abcd value >>', abcd)

// var place = 'bhaktapur';
// // template literals
// // string concatinate 'slfjd'+'dlfjldkf'+ dd+'lsdkfjld'
// var nameis = `welcome 
// to  ${place}`

// console.log('name is >>', nameis)


// import and export

// export syntax (es6)
// two ways of export in es6
// named export
// syntax
// export const something = {};
// export const newThing = [];
// export class Fruits {

// }
// NOTE ==> THERE CAN BE MULTIPLE NAMED EXPORT

// default export
// NOTE ==> THERE CAN BE ONLY ONE DEFAULT EXPORT
// syntax
// var a;
// export default a;
// there can be default export as well as multiple named export in a file

// import syntax
// import totally depends on how it is exported
// when it is named export
// import { something, } from './location of file';

// when it is default export
// import anyName from './location of file';

// when there is both named export and default export
// import abcdxyz,{a,b} from './location of file'

// class
// class is group of constructor methods and properties
// user defined data type
// item
// 

// syntax
class Notification {
    status = 'online'

    constructor(x) {

    }

    add() {

    }

    subtract() {

    }
}

// inheritance
// base class (parent)
// dereived class (child)

class Reviews extends Notification {

    constructor() {
        super();  // super call is parent class constructor call
    }

    getStatus() {
        return this.status;
    }
}

var abc = new Reviews();
console.log('abc ...',abc.getStatus())